import React from "react";
import classes from "../styles/Home.module.css";
import { useRouter } from "next/router";

const Home = () => {
  const router = useRouter();

  return (
    <div className={classes.indexContainer}>
      <button onClick={() => router.push("/mint")} className={classes.button}>
        Go to minting
      </button>
    </div>
  );
};

export default Home;
